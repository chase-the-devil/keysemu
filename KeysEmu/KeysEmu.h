#include <Windows.h>
#include <stdio.h>

//defines

#define		WND_DEF_WDH					500
#define		WND_DEF_HGH					400

#define		FLIN_KEY_PRESS				0x01
#define		FLIN_KEY_RELEASE			0x02

#define		JUMP_KEY_INDEX				0x00
#define		USE_KEY_INDEX				0x01
#define		ATTACK_KEY_INDEX			0x02
#define		INVNEXT_KEY_INDEX			0x03
#define		INVPREV_KEY_INDEX			0x04
#define		SLOT2_KEY_INDEX				0x05
#define		EMU_KEYS_LEN				0x06

#define		L4D2_KEY_TAGS_LEN			69

#define		BHOP_BND_KEY_INDEX			0
#define		JROCK_BND_KEY_INDEX			1
#define		FMELEE_BND_KEY_INDEX		2

#define		BHOP_ON_KEY_INDEX			3
#define		BHOP_OFF_KEY_INDEX			4

#define		JROCK_ON_KEY_INDEX			5
#define		JROCK_OFF_KEY_INDEX			6

#define		FMELEE_ON_KEY_INDEX			7
#define		FMELEE_OFF_KEY_INDEX		8

#define		BND_KEYS_LEN				9

#define		BND_ADDITIONAL_LEN			5

#define		BND_ALL_PARAMS_LEN			BND_KEYS_LEN + BND_ADDITIONAL_LEN

#define		BIND_TYPE_USER_ACTION		0x00
#define		BIND_TYPE_USER_KEY			0x01
#define		BIND_TYPE_NORMAL			0x00
#define		BIND_TYPE_SWITCH			0x02

#define		L4D2_BIND_TYPE_INDEX_NORMAL (char)(0xFF)
#define		L4D2_BIND_TYPE_INDEX_SWITCH (char)(0x00)

#define		SWITCH_BHOP_MASK			(short)(0x0001)
#define		SWITCH_JROCK_MASK			(short)(0x0002)
#define		SWITCH_FMELEE_MASK			(short)(0x0004)
#define		SWITCH_BHOP_ON_MASK			(short)(0x0008)
#define		SWITCH_BHOP_OFF_MASK		(short)(0x0010)
#define		SWITCH_JROCK_ON_MASK		(short)(0x0020)
#define		SWITCH_JROCK_OFF_MASK		(short)(0x0040)
#define		SWITCH_FMELEE_ON_MASK		(short)(0x0080)
#define		SWITCH_FMELEE_OFF_MASK		(short)(0x0100)

#define		SWITCH_OLD_LEVEL			0
#define		SWITCH_NEW_LEVEL			1

#define		EXTRA_INFO_CODE				9

#define		L4D2_SUB_FOLDERS_LENGTH		26

#pragma		pack(1)
struct RGBAPix
	{
		unsigned __int8 b;
		unsigned __int8 g;
		unsigned __int8 r;
		unsigned __int8 a;
	};

struct L4D2Tag
	{
		char* const			Tag;
		DWORD				type;
		DWORD				Data;
		struct
			{
				DWORD	dwDown;
				DWORD	dwUp;
			}				dwFlags;
	};

struct L4D2BindEvents
	{
		unsigned char		eventsCount;
		INPUT*				events;
	};

struct L4D2Code
	{
		DWORD				code;
		char				index;
		unsigned char		eventCount;
		short*				events;
	};

struct L4D2CodeSeq	
	{
		unsigned int		codeLength;
		struct L4D2Code*	codeSeq;
	};

//***	globals

HWND				hMainWindow				= NULL;
RECT				rect;
HDC					hdc;
HDC					memdc;
HBITMAP				membmp;
struct RGBAPix*		pixels;

HHOOK				hookKey;

//**	L4D2 globals

//*		string constants

const char 			L4D2SubFolders[L4D2_SUB_FOLDERS_LENGTH]		= "left4dead2\\cfg\\config.cfg";
const char*			L4D2ErrorCaption							= "Error occurred";
const char*			L4D2ErrorLocal								= "File \"config.cfg\" couldn't open!\nTry to check local \"config.ecfg\".";
const char*			L4D2ErrorOuter								= "File \"config.cfg\" couldn't open!\nTry to check \nBIND L4D2_FOLDER <>\nBIND CFG_PATH <>\ncommands in local \"config.ecfg\"\nfor correct game config path.";
const char*			L4D2ErrorNoAction							= "Some actions needed for macro wasn't bind in game:\n";
const char*			L4D2WarningUserBindsCaption					= "Warning";
const char*			L4D2WarningUserBinds						= "Not all actions have been bind!";

const char*			L4D2_jump									= "+JUMP";
const char*			L4D2_use									= "+USE";
const char*			L4D2_attack									= "+ATTACK";
const char*			L4D2_invnext								= "INVNEXT";
const char*			L4D2_invprev								= "INVPREV";
const char*			L4D2_slot2									= "SLOT2";


//*		globals

char*					cfgPath									= NULL;

struct L4D2BindEvents	emuKeys[EMU_KEYS_LEN];
struct L4D2CodeSeq		bndKeys									= {0, NULL};

int						bhopDelay								= 18;
int						jumpRockDelay							= 9;
int						meleeDelay								= 14;
int						afterMeleeAttackDelay					= 500;

volatile __int8			fastMeleeOn								= 1;
volatile __int8			bHopOn									= 1;
volatile __int8			jRockOn									= 1;

volatile __int8			threadBhopDead							= 1;
volatile __int8			keyBhopState							= 0;
volatile __int8			threadMeleeDead							= 1;
volatile __int8			keyMeleeState							= 0;

int						renderPos								= 0;


HANDLE					bhopMutex;
HANDLE					fmeleeMutex;

const struct L4D2Tag L4D2KeyTags[L4D2_KEY_TAGS_LEN] =
	{
		//L4D2_TAG				TYPE				V_CODE			FLAGS

		//single-char tags(besides letters)

		{	";",				INPUT_KEYBOARD,		0x000000ba,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"=",				INPUT_KEYBOARD,		0x000000bb,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	",",				INPUT_KEYBOARD,		0x000000bc,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"-",				INPUT_KEYBOARD,		0x000000bd,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	".",				INPUT_KEYBOARD,		0x000000be,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"\\",				INPUT_KEYBOARD,		0x000000bf,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"`",				INPUT_KEYBOARD,		0x000000c0,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"[",				INPUT_KEYBOARD,		0x000000db,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"]",				INPUT_KEYBOARD,		0x000000dd,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"/",				INPUT_KEYBOARD,		0x000000dc,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"'",				INPUT_KEYBOARD,		0x000000de,		{	0x00000000,				KEYEVENTF_KEYUP			}	},

		//multi-char tags
		{	"SPACE",			INPUT_KEYBOARD,		0x00000020,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"BACKSPACE",		INPUT_KEYBOARD,		0x00000008,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"TAB",				INPUT_KEYBOARD,		0x00000009,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"PAUSE",			INPUT_KEYBOARD,		0x00000013,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"CAPSLOCK",			INPUT_KEYBOARD,		0x00000014,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"ESCAPE",			INPUT_KEYBOARD,		0x0000001B,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"SHIFT",			INPUT_KEYBOARD,		0x000000A0,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"RSHIFT",			INPUT_KEYBOARD,		0x000000A1,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"CTRL",				INPUT_KEYBOARD,		0x000000A2,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"RCTRL",			INPUT_KEYBOARD,		0x000000A3,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"ALT",				INPUT_KEYBOARD,		0x000000A4,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"RALT",				INPUT_KEYBOARD,		0x000000A5,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"LEFTARROW",		INPUT_KEYBOARD,		0x00000025,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"UPARROW",			INPUT_KEYBOARD,		0x00000026,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"RIGHTARROW",		INPUT_KEYBOARD,		0x00000027,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"DOWNARROW",		INPUT_KEYBOARD,		0x00000028,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F1",				INPUT_KEYBOARD,		0x00000070,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F2",				INPUT_KEYBOARD,		0x00000071,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F3",				INPUT_KEYBOARD,		0x00000072,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F4",				INPUT_KEYBOARD,		0x00000073,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F5",				INPUT_KEYBOARD,		0x00000074,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F6",				INPUT_KEYBOARD,		0x00000075,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F7",				INPUT_KEYBOARD,		0x00000076,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F8",				INPUT_KEYBOARD,		0x00000077,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F9",				INPUT_KEYBOARD,		0x00000078,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F10",				INPUT_KEYBOARD,		0x00000079,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F11",				INPUT_KEYBOARD,		0x0000007A,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"F12",				INPUT_KEYBOARD,		0x0000007B,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_INS",			INPUT_KEYBOARD,		0x00000060,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_END",			INPUT_KEYBOARD,		0x00000061,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_DOWNARROW",		INPUT_KEYBOARD,		0x00000062,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_PGDN",			INPUT_KEYBOARD,		0x00000063,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_LEFTARROW",		INPUT_KEYBOARD,		0x00000064,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_5",				INPUT_KEYBOARD,		0x00000065,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_RIGHTARROW",	INPUT_KEYBOARD,		0x00000066,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_HOME",			INPUT_KEYBOARD,		0x00000067,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_UPARROW",		INPUT_KEYBOARD,		0x00000068,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_PGUP",			INPUT_KEYBOARD,		0x00000069,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_MULTIPLY",		INPUT_KEYBOARD,		0x0000006A,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_PLUS",			INPUT_KEYBOARD,		0x0000006B,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_SLASH",			INPUT_KEYBOARD,		0x0000006C,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_MINUS",			INPUT_KEYBOARD,		0x0000006D,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_DEL",			INPUT_KEYBOARD,		0x0000006E,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"KP_ENTER",			INPUT_KEYBOARD,		0x0000000D,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"ENTER",			INPUT_KEYBOARD,		0x0000000D,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"PGUP",				INPUT_KEYBOARD,		0x00000021,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"PGDN",				INPUT_KEYBOARD,		0x00000022,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"END",				INPUT_KEYBOARD,		0x00000023,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"HOME",				INPUT_KEYBOARD,		0x00000024,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"INS",				INPUT_KEYBOARD,		0x0000002D,		{	0x00000000,				KEYEVENTF_KEYUP			}	},
		{	"DELETE",			INPUT_KEYBOARD,		0x0000002E,		{	0x00000000,				KEYEVENTF_KEYUP			}	},

		//L4D2_TAG				TYPE				MOUSE_DATA			FLAGS

		{	"MOUSE1",			INPUT_MOUSE,		0x00000000,		{	MOUSEEVENTF_LEFTDOWN,	MOUSEEVENTF_LEFTUP		}	},
		{	"MOUSE2",			INPUT_MOUSE,		0x00000000,		{	MOUSEEVENTF_RIGHTDOWN,	MOUSEEVENTF_RIGHTUP		}	},
		{	"MOUSE3",			INPUT_MOUSE,		0x00000000,		{	MOUSEEVENTF_MIDDLEDOWN,	MOUSEEVENTF_MIDDLEUP	}	},
		{	"MOUSE4",			INPUT_MOUSE,		XBUTTON1,		{	MOUSEEVENTF_XDOWN,		MOUSEEVENTF_XUP			}	},
		{	"MOUSE5",			INPUT_MOUSE,		XBUTTON2,		{	MOUSEEVENTF_XDOWN,		MOUSEEVENTF_XUP			}	},
		{	"MWHEELUP",			INPUT_MOUSE,		-WHEEL_DELTA,	{	MOUSEEVENTF_WHEEL,		MOUSEEVENTF_WHEEL		}	},
		{	"MWHEELDOWN",		INPUT_MOUSE,		WHEEL_DELTA,	{	MOUSEEVENTF_WHEEL,		MOUSEEVENTF_WHEEL		}	}
	};

//prototypes

int		__stdcall	WinMain				(
											HINSTANCE	hInst,
											HINSTANCE	hPreviousInst,
											LPSTR		lpCommandLine,
											int			nCommandShow
										);

LRESULT __stdcall	WindowProc			(
											HWND	hWnd,
											UINT	uMsg,
											WPARAM	wParam,
											LPARAM	lParam
										);

int					readL4D2Config		(char* confPath, char configType, int configLength		);
char				doBind				(char bindType, char level, char* key, char* action		);
char				keyL4D2TagToInput	(char* inTag, short inIndex								);
char				keyL4D2TagToVCode	(char* inTag, short inIndex, char bindType, char level	);
char				vCodeToL4D2CodeSeq	(DWORD inCode, short inIndex, char inBindIndex, char level);
void				toUpperCase			(char* inChar											);
void				freeL4D2CodeSeq		(struct L4D2CodeSeq* inSeq								);


LRESULT __stdcall	threadKeyHook		(int messageCode, WPARAM wParam, LPARAM lParam			);

DWORD	__stdcall	bhopThread			(LPVOID inParam											);
DWORD	__stdcall	jumpRockThread		(LPVOID inParam											);
DWORD	__stdcall	meleeThread			(LPVOID inParam											);