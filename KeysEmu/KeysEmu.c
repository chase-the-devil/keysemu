#include "KeysEmu.h"

int		__stdcall	WinMain		(
									HINSTANCE	hInst,
									HINSTANCE	hPreviousInst,
									LPSTR		lpCommandLine,
									int			nCommandShow
								)
	{
		bhopMutex				= CreateMutexW(NULL, 0, NULL);
		fmeleeMutex				= CreateMutexW(NULL, 0, NULL);

		//BHop - T30_surv: 56 msec; T100_surv: 22 msec; T100_tank: 18 msec.

		memset(emuKeys, 0, sizeof(struct L4D2BindEvents)	* EMU_KEYS_LEN);

		readL4D2Config("config.ecfg", BIND_TYPE_USER_ACTION, BND_ALL_PARAMS_LEN);

		if(!readL4D2Config(cfgPath, BIND_TYPE_USER_KEY, EMU_KEYS_LEN))
			{
				char* msg[500];
				strcpy(msg, L4D2ErrorNoAction);

				for(unsigned int index = 0; index < EMU_KEYS_LEN; ++index)
					{
						if(emuKeys[index].eventsCount == 0)
							{
								switch(index)
									{
										case JUMP_KEY_INDEX:
											{
												strcat(msg, L4D2_jump);
												break;
											}
										case USE_KEY_INDEX:
											{
												strcat(msg, L4D2_use);
												break;
											}
										case ATTACK_KEY_INDEX:
											{
												strcat(msg, L4D2_attack);
												break;
											}
										case INVNEXT_KEY_INDEX:
											{
												strcat(msg, L4D2_invnext);
												break;
											}
										case INVPREV_KEY_INDEX:
											{
												strcat(msg, L4D2_invprev);
												break;
											}
										case SLOT2_KEY_INDEX:
											{
												strcat(msg, L4D2_slot2);
											}
									}
							}
					}

				MessageBoxA	(
								hMainWindow,
								msg,
								L4D2ErrorCaption,
								MB_OK
							);

				goto __final;
			}

		WNDCLASSEX	windowClassStruct;
		
		windowClassStruct.cbSize		= sizeof(windowClassStruct);
		windowClassStruct.style			= CS_BYTEALIGNCLIENT;
		windowClassStruct.lpfnWndProc	= WindowProc;
		windowClassStruct.cbClsExtra	= 0;
		windowClassStruct.cbWndExtra	= 0;
		windowClassStruct.hInstance		= hInst;
		windowClassStruct.hIcon			= NULL;
		windowClassStruct.hCursor		= LoadCursorW(NULL, IDC_ARROW);
		windowClassStruct.hbrBackground = (HBRUSH)COLOR_APPWORKSPACE;
		windowClassStruct.lpszMenuName	= NULL;
		windowClassStruct.hIconSm		= NULL;
		windowClassStruct.lpszClassName = L"Emu";

		if(!RegisterClassExW(&windowClassStruct))
			return 0;

		hMainWindow = CreateWindowExW	(
											WS_OVERLAPPED,
											L"Emu",
											L"KeysEmu",
											WS_OVERLAPPEDWINDOW,
											abs(GetSystemMetrics(SM_CXSCREEN)-WND_DEF_WDH) / 2,
											abs(GetSystemMetrics(SM_CYSCREEN)-WND_DEF_HGH) / 2,
											WND_DEF_WDH,
											WND_DEF_HGH,
											NULL,
											NULL,
											hInst,
											NULL
										);

		if(!hMainWindow)
			return 0;			

		ShowWindow	(hMainWindow, nCommandShow);
		UpdateWindow(hMainWindow);

		SetWindowsHookExW(WH_KEYBOARD_LL, threadKeyHook, GetModuleHandleW(NULL), (DWORD)(0));

		MSG msg;

		while(GetMessageW(&msg, NULL, 0, 0))
			{
				TranslateMessage(&msg);
				DispatchMessageW(&msg);
			}

		UnhookWindowsHookEx(hookKey);

		UnregisterClassW(L"Emu", windowClassStruct.hInstance);

	__final:

		CloseHandle(bhopMutex);
		CloseHandle(fmeleeMutex);

		return 0;
	}

LRESULT __stdcall threadKeyHook(int messageCode, WPARAM wParam, LPARAM lParam)
	{
		if(messageCode == HC_ACTION && ((KBDLLHOOKSTRUCT*)lParam)->dwExtraInfo != EXTRA_INFO_CODE)
			{
				switch(wParam)
					{
						case WM_KEYDOWN:
						case WM_SYSKEYDOWN:
							{
								unsigned int	index	= 0;

								while(index < bndKeys.codeLength)
									{
										if(((KBDLLHOOKSTRUCT*)lParam)->vkCode == bndKeys.codeSeq[index].code)
											break;
						
										++index;
									}

								if(index < bndKeys.codeLength)
									{
										char checked = 0;

 										if(bndKeys.codeSeq[index].index == L4D2_BIND_TYPE_INDEX_NORMAL)
											{
												for(int cmdIndex = 0; cmdIndex < bndKeys.codeSeq[index].eventCount; ++cmdIndex)
													{
														switch(bndKeys.codeSeq[index].events[cmdIndex])
															{
																case BHOP_BND_KEY_INDEX:
																	{
																		if(bHopOn)
																			{
																				WaitForSingleObject(bhopMutex, INFINITE);

																				if(threadBhopDead)
																					{
																						threadBhopDead = 0;
																						keyBhopState = 1;

																						CreateThread(NULL, 0, bhopThread, NULL, 0, NULL);

																						checked = 1;
																					}

																				ReleaseMutex(bhopMutex);
																			}

																		break;
																	}
																case JROCK_BND_KEY_INDEX:
																	{
																		if(jRockOn)
																			{
																				CreateThread(NULL, 0, jumpRockThread, NULL, 0, NULL);

																				checked = 1;
																			}

																		break;
																	}
																case FMELEE_BND_KEY_INDEX:
																	{
																		if(fastMeleeOn)
																			{
																				WaitForSingleObject(fmeleeMutex, INFINITE);

																				if(threadMeleeDead)
																					{
																						threadMeleeDead = 0;
																						keyMeleeState = 1;

																						CreateThread(NULL, 0, meleeThread, NULL, 0, NULL);
																					}

																				ReleaseMutex(fmeleeMutex);

																				checked = 1;
																			}

																		break;
																	}
																case BHOP_ON_KEY_INDEX:
																	{
																		bHopOn = 1;

																		break;
																	}
																case BHOP_OFF_KEY_INDEX:
																	{
																		bHopOn = 0;

																		break;
																	}
																case JROCK_ON_KEY_INDEX:
																	{
																		jRockOn = 1;
																		
																		break;
																	}
																case JROCK_OFF_KEY_INDEX:
																	{
																		jRockOn = 0;

																		break;
																	}
																case FMELEE_ON_KEY_INDEX:
																	{
																		fastMeleeOn = 1;
																		
																		break;
																	}
																case FMELEE_OFF_KEY_INDEX:
																	{
																		fastMeleeOn = 0;
																	}
															}
													}

												if(checked)
													return TRUE;
											}
										else
											{
												int cmdIndex = bndKeys.codeSeq[index].index;

												++bndKeys.codeSeq[index].index;
														
												if(bndKeys.codeSeq[index].index >= bndKeys.codeSeq[index].eventCount)
													bndKeys.codeSeq[index].index = 0;

												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_MASK)
													{
														if(bHopOn)
															{
																WaitForSingleObject(bhopMutex, INFINITE);

																if(threadBhopDead)
																	{
																		threadBhopDead = 0;
																		keyBhopState = 1;

																		CreateThread(NULL, 0, bhopThread, NULL, 0, NULL);
																	}

																ReleaseMutex(bhopMutex);

																checked = 1;
															}
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_JROCK_MASK)
													{
														if(jRockOn)
															{
																CreateThread(NULL, 0, jumpRockThread, NULL, 0, NULL);

																checked = 1;
															}
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_MASK)
													{
														if(fastMeleeOn)
															{
																WaitForSingleObject(fmeleeMutex, INFINITE);

																if(threadMeleeDead)
																	{
																		threadMeleeDead = 0;
																		keyMeleeState = 1;

																		CreateThread(NULL, 0, meleeThread, NULL, 0, NULL);
																	}

																ReleaseMutex(fmeleeMutex);

																checked = 1;
															}
													}
												
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_ON_MASK)
													bHopOn = 1;
												
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_OFF_MASK)
													bHopOn = 0;

												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_JROCK_ON_MASK)
													jRockOn = 1;				

												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_JROCK_OFF_MASK)
													jRockOn = 0;

												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_ON_MASK)
													fastMeleeOn = 1;
																		
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_OFF_MASK)
													fastMeleeOn = 0;

												if(checked)
													return TRUE;
											}
									}

								break;
							}
						case WM_KEYUP:
						case WM_SYSKEYUP:
							{
								unsigned int	index	= 0;

								while(index < bndKeys.codeLength)
									{
										if(((KBDLLHOOKSTRUCT*)lParam)->vkCode == bndKeys.codeSeq[index].code)
											break;
						
										++index;
									}

								if(index < bndKeys.codeLength)
									{
										if(bndKeys.codeSeq[index].index == L4D2_BIND_TYPE_INDEX_NORMAL)
											{
												for(int cmdIndex = 0; cmdIndex < bndKeys.codeSeq[index].eventCount; ++cmdIndex)
													{
														switch(bndKeys.codeSeq[index].events[cmdIndex])
															{
																case BHOP_BND_KEY_INDEX:
																	{
																		WaitForSingleObject(bhopMutex, INFINITE);

																		keyBhopState = 0;

																		ReleaseMutex(bhopMutex);

																		break;
																	}
																case FMELEE_BND_KEY_INDEX:
																	{
																		WaitForSingleObject(fmeleeMutex, INFINITE);

																		keyMeleeState = 0;

																		ReleaseMutex(fmeleeMutex);
																	}
															}
													}
											}
										else
											{
												for(int cmdIndex = 0; cmdIndex < bndKeys.codeSeq[index].eventCount; ++cmdIndex)
													{
														if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_MASK)
															{
																WaitForSingleObject(bhopMutex, INFINITE);

																keyBhopState = 0;

																ReleaseMutex(bhopMutex);
															}
														if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_MASK)
															{
																WaitForSingleObject(fmeleeMutex, INFINITE);

																keyMeleeState = 0;

																ReleaseMutex(fmeleeMutex);
															}
													}
											}
									}
							}
					}
			}

		return CallNextHookEx(hookKey, messageCode, wParam, lParam);
	}

DWORD	__stdcall	meleeThread(LPVOID inParam)
	{
		SendInput	(
						emuKeys[SLOT2_KEY_INDEX].eventsCount,
						emuKeys[SLOT2_KEY_INDEX].events,
						sizeof(INPUT)
					);
		Sleep(meleeDelay);

		__label:

			WaitForSingleObject(fmeleeMutex, INFINITE);

			if(keyMeleeState == 0)
				{
					ReleaseMutex(fmeleeMutex);
					threadMeleeDead = 1;
					return 0;
				}

			ReleaseMutex(fmeleeMutex);

			SendInput	(
							emuKeys[ATTACK_KEY_INDEX].eventsCount,
							emuKeys[ATTACK_KEY_INDEX].events,
							sizeof(INPUT)
						);
			Sleep(meleeDelay);

			SendInput	(
							emuKeys[INVPREV_KEY_INDEX].eventsCount,
							emuKeys[INVPREV_KEY_INDEX].events,
							sizeof(INPUT)
						);
			Sleep(meleeDelay);

			SendInput	(
							emuKeys[INVNEXT_KEY_INDEX].eventsCount,
							emuKeys[INVNEXT_KEY_INDEX].events,
							sizeof(INPUT)
						);
			Sleep(afterMeleeAttackDelay);

		goto __label;

		return 0;
	}

DWORD __stdcall jumpRockThread(LPVOID inParam)
	{
		SendInput(emuKeys[USE_KEY_INDEX].eventsCount, emuKeys[USE_KEY_INDEX].events, sizeof(INPUT));
		Sleep(jumpRockDelay);

		SendInput(emuKeys[JUMP_KEY_INDEX].eventsCount, emuKeys[JUMP_KEY_INDEX].events, sizeof(INPUT));
		Sleep(jumpRockDelay);

		return 0;
	}

DWORD __stdcall bhopThread(LPVOID inParam)
	{
		__label:

			WaitForSingleObject(bhopMutex, INFINITE);

			if(keyBhopState == 0)
				{
					ReleaseMutex(bhopMutex);
					threadBhopDead = 1;
					return 0;
				}

			ReleaseMutex(bhopMutex);

       		SendInput(emuKeys[JUMP_KEY_INDEX].eventsCount, emuKeys[JUMP_KEY_INDEX].events,	sizeof(INPUT));
			Sleep(bhopDelay);
		
		goto __label;

		return 0;
	}

int	readL4D2Config(char* confPath, char configType, int configLength)
	{
		FILE*	conf;
		char	buf[255];
		char*	curr;
		char*	base;
		char*	param1st;
		char	bindCount	= 0;
		char	bindType;
		char	level;

		if(confPath == NULL || (conf = fopen(confPath, "r")) == NULL)
			{
				if(configType == BIND_TYPE_USER_ACTION)
					{
						MessageBoxA	(
										hMainWindow,
										L4D2ErrorLocal,
										L4D2ErrorCaption,
										MB_OK
									);

						return 1;
					}
				
				if(configType == BIND_TYPE_USER_KEY)
					{
						MessageBoxA	(
										hMainWindow,
										L4D2ErrorOuter,
										L4D2ErrorCaption,
										MB_OK
									);

						return 1;
					}

				return 1;
			}

		while(!feof(conf))
			{
				curr = buf;

				base = NULL;
				param1st = NULL;

				fgets(buf, 255, conf);
				
				if(buf[0] == '/' && buf[1] == '/')
					continue;

				while(*curr && (*curr == ' ' || *curr == '	'))
					curr = (char*)((int)(curr) + sizeof(char));

				if(*curr == '\n')
					continue;

				base = curr;

				while(*curr && *curr != ' ' && *curr != '	' && *curr != '\n')
					curr = (char*)((int)(curr) + sizeof(char));

				if(*curr == '\n')
					continue;

				*curr = 0;

				curr = (char*)((int)(curr) + sizeof(char));

				toUpperCase(base);

				if(strcmp(base, "BIND") == 0)
					bindType = BIND_TYPE_NORMAL;
				else if(strcmp(base, "BIND_SWITCH") == 0)
					{
						bindType = BIND_TYPE_SWITCH;
						level = SWITCH_NEW_LEVEL;
					}
				else
					continue;

				while(*curr && (*curr == ' ' || *curr == '	'))
					curr = (char*)((int)(curr) + sizeof(char));

				if(*curr == '\n')
					continue;

				if(*curr == '"')
					{
						curr = (char*)((int)(curr) + sizeof(char));

						param1st = curr;

						while(*curr && *curr != '"' && *curr != '\n')
							curr = (char*)((int)(curr) + sizeof(char));
					}
				else
					{
						param1st = curr;

						while(*curr && *curr != '"' && *curr != ' ' && *curr != '	' && *curr != '\n')
							curr = (char*)((int)(curr) + sizeof(char));
					}

				if(*curr == '\n')
					continue;

				*curr = 0;

			__arg2_loop:

					curr = (char*)((int)(curr) + sizeof(char));

					if(*curr == '\n' || *curr == 0)
						continue;

					while(*curr && (*curr == ' ' || *curr == '	'))
						curr = (char*)((int)(curr) + sizeof(char));

					if(*curr == '\n' || *curr == 0)
						continue;

					if(bindType == BIND_TYPE_SWITCH && *curr == ';')
						{
							if(base)
								{
									level = SWITCH_NEW_LEVEL;

									curr = (char*)((int)(curr) + sizeof(char));

									if(*curr == '\n' || *curr == 0)
										continue;

									while(*curr && (*curr == ' ' || *curr == '	'))
										curr = (char*)((int)(curr) + sizeof(char));

									if(*curr == '\n' || *curr == 0)
										continue;
								}
							else
								continue;
						}

					if(*curr == '"')
						{
							curr = (char*)((int)(curr) + sizeof(char));

							base = curr;

							while(*curr && *curr != '"' && *curr != '\n')
								curr = (char*)((int)(curr) + sizeof(char));
						}
					else
						{
							base = curr;

							while(*curr && *curr != '"' && *curr != ' ' && *curr != '	' && *curr != '\n' && *curr != ';')
								curr = (char*)((int)(curr) + sizeof(char));
						}

					if(*curr == ';')
						{
							*curr = 0;

							bindCount += doBind(configType | bindType, level, param1st, base);

							level = SWITCH_NEW_LEVEL;
						}
					else
						{
							*curr = 0;

							bindCount += doBind(configType | bindType, level, param1st, base);

							level = SWITCH_OLD_LEVEL;
						}

				goto __arg2_loop;
			}

		fclose(conf);

		if(bindCount >= configLength)
			return 1;
		else
			return 0;
	}

char doBind(char bindType, char level, char* key, char* action)
	{
		toUpperCase(key);

		if(bindType & BIND_TYPE_USER_KEY)
			{
				toUpperCase(action);

				if(strcmp(action, L4D2_jump) == 0)
					return keyL4D2TagToInput(key, JUMP_KEY_INDEX);

				if(strcmp(action, L4D2_use) == 0)
					return keyL4D2TagToInput(key, USE_KEY_INDEX);

				if(strcmp(action, L4D2_attack) == 0)
					return keyL4D2TagToInput(key, ATTACK_KEY_INDEX);

				if(strcmp(action, L4D2_invnext) == 0)
					return keyL4D2TagToInput(key, INVNEXT_KEY_INDEX);

				if(strcmp(action, L4D2_invprev) == 0)
					return keyL4D2TagToInput(key, INVPREV_KEY_INDEX);

				if(strcmp(action, L4D2_slot2) == 0)
					return keyL4D2TagToInput(key, SLOT2_KEY_INDEX);

				return 0;
			}

		if(strcmp(key, "BUNNY_HOP_DELAY") == 0)
			{
				bhopDelay = atoi(action);

				return 1;
			}
		if(strcmp(key, "AFTER_MELEE_ATTACK_DELAY") == 0)
			{
				afterMeleeAttackDelay = atoi(action);

				return 1;
			}
		if(strcmp(key, "FAST_MELEE_DELAY") == 0)
			{
				meleeDelay = atoi(action);

				return 1;
			}
		if(strcmp(key, "JUMP_ROCK_DELAY") == 0)
			{
				jumpRockDelay = atoi(action);

				return 1;
			}
		if(strcmp(key, "L4D2_FOLDER") == 0)
			{
				int keyLen = strlen(action);

				if(cfgPath)
					free(cfgPath);

				cfgPath = (char*) malloc((keyLen + L4D2_SUB_FOLDERS_LENGTH) * sizeof(char));

				strcpy(cfgPath, action);
				strcat(cfgPath, L4D2SubFolders);

				return 1;
			}

		if(strcmp(key, "CFG_PATH") == 0)
			{
				int keyLen = strlen(action);

				if(cfgPath)
					free(cfgPath);

				cfgPath = (char*) malloc(keyLen);

				strcpy(cfgPath, action);

				return 1;
			}

		toUpperCase(action);

		if(strcmp(action, "BUNNY_HOP") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_BHOP_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, BHOP_BND_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "JUMP_ROCK") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_JROCK_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, JROCK_BND_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "FAST_MELEE") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_FMELEE_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, FMELEE_BND_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "BUNNY_HOP_ON") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_BHOP_ON_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, BHOP_ON_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "BUNNY_HOP_OFF") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_BHOP_OFF_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, BHOP_OFF_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "JUMP_ROCK_ON") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_JROCK_ON_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, JROCK_ON_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "JUMP_ROCK_OFF") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_JROCK_OFF_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, JROCK_OFF_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "FAST_MELEE_ON") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_FMELEE_ON_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, FMELEE_ON_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		if(strcmp(action, "FAST_MELEE_OFF") == 0)
			{
				if(bindType & BIND_TYPE_SWITCH)
					return keyL4D2TagToVCode(key, SWITCH_FMELEE_OFF_MASK, L4D2_BIND_TYPE_INDEX_SWITCH, level);
				else
					return keyL4D2TagToVCode(key, FMELEE_OFF_KEY_INDEX, L4D2_BIND_TYPE_INDEX_NORMAL, SWITCH_OLD_LEVEL);
			}

		return 0;
	}

char keyL4D2TagToVCode(char* inTag, short inIndex, char bindType, char level)
	{
		if(*(char*)((int)(inTag) + sizeof(char)) == 0 && *inTag >= 0x41 && *inTag <= 0x5a) //65-90
			return vCodeToL4D2CodeSeq(*inTag, inIndex, bindType, level);
		else
			{
				for(unsigned short index = 0; index < L4D2_KEY_TAGS_LEN; ++index)
					{
						if(strcmp(inTag, L4D2KeyTags[index].Tag) == 0 && L4D2KeyTags[index].type == INPUT_KEYBOARD)
							return vCodeToL4D2CodeSeq(L4D2KeyTags[index].Data, inIndex, bindType, level);
					}

				return 0;
			}
	}

char vCodeToL4D2CodeSeq(DWORD inCode, short inIndex, char inBindIndex, char level)
	{
		for(unsigned int index = 0; index < bndKeys.codeLength; ++index)
			{
				if(bndKeys.codeSeq[index].code == inCode)
					{
						if(inBindIndex == L4D2_BIND_TYPE_INDEX_NORMAL)
							{
								for(int locIndex = 0; locIndex < bndKeys.codeSeq[index].eventCount; ++locIndex)
									{
										if(bndKeys.codeSeq[index].events[locIndex] == inIndex)
											return 0;
									}
							}
						else
							{
								if(level == SWITCH_OLD_LEVEL)
									{
										if	(
												bndKeys.codeSeq[index].events	[
																					bndKeys.codeSeq[index].eventCount - 1
																				]	&
												SWITCH_BHOP_MASK					&
												SWITCH_JROCK_MASK					&
												SWITCH_FMELEE_MASK					&
												SWITCH_BHOP_ON_MASK					&
												SWITCH_BHOP_OFF_MASK				&
												SWITCH_JROCK_ON_MASK				&
												SWITCH_JROCK_OFF_MASK				&
												SWITCH_FMELEE_ON_MASK				&
												SWITCH_FMELEE_OFF_MASK
											)
											return 0;

										bndKeys.codeSeq[index].events[bndKeys.codeSeq[index].eventCount - 1] |= inIndex;

										return 1;
									}
							}

						short* tmpEvents = (short*) malloc(sizeof(short) * (bndKeys.codeSeq[index].eventCount + 1));

						if(bndKeys.codeSeq[index].events)
							{
								memcpy(tmpEvents, bndKeys.codeSeq[index].events, bndKeys.codeSeq[index].eventCount * sizeof(short));
								free(bndKeys.codeSeq[index].events);
							}

						bndKeys.codeSeq[index].index											= inBindIndex;
						bndKeys.codeSeq[index].events											= tmpEvents;
						bndKeys.codeSeq[index].events[(bndKeys.codeSeq[index].eventCount++)]	= inIndex;

						return 1;
					}
			}

		struct L4D2Code*	tmpCodeSeq = (struct L4D2Code*) malloc(sizeof(struct L4D2Code) * (bndKeys.codeLength + 1));

		if(bndKeys.codeSeq)
			{
				memcpy(tmpCodeSeq, bndKeys.codeSeq, sizeof(struct L4D2Code) * bndKeys.codeLength);
				free(bndKeys.codeSeq);
			}

		bndKeys.codeSeq = tmpCodeSeq;

		bndKeys.codeSeq[bndKeys.codeLength].code		= inCode;
		bndKeys.codeSeq[bndKeys.codeLength].eventCount	= 1;
		bndKeys.codeSeq[bndKeys.codeLength].index		= inBindIndex;
		bndKeys.codeSeq[bndKeys.codeLength].events		= (short*) malloc(sizeof(short));
		bndKeys.codeSeq[bndKeys.codeLength].events[0]	= inIndex;
		++bndKeys.codeLength;

		return 1;
	}

void freeL4D2CodeSeq(struct L4D2CodeSeq* inSeq)
	{
		if(inSeq->codeSeq)
			{
				for(unsigned int index = 0; index < inSeq->codeLength; ++index)
					{
						if(inSeq->codeSeq[index].events)
							free(inSeq->codeSeq[index].events);
					}

				free(inSeq->codeSeq);
			}

		inSeq->codeSeq		= NULL;
		inSeq->codeLength	= 0;
	}

char keyL4D2TagToInput(char* inTag, short inIndex)
	{
		if(emuKeys[inIndex].events != NULL)
			return 0;

		if(*(char*)((int)(inTag) + sizeof(char)) == 0 && ((*inTag >= 0x41 && *inTag <= 0x5a) || (*inTag >= 0x30 && *inTag <= 0x3a))) //65-90, 48-58
			{
				emuKeys[inIndex].eventsCount				= 2;
				emuKeys[inIndex].events						= (INPUT*) malloc(2 * sizeof(INPUT));

				emuKeys[inIndex].events[0].type				= INPUT_KEYBOARD;
				emuKeys[inIndex].events[0].ki.wVk			= *inTag;
				emuKeys[inIndex].events[0].ki.wScan			= MapVirtualKeyW(*inTag, MAPVK_VK_TO_VSC);
				emuKeys[inIndex].events[0].ki.dwFlags		= 0;
				emuKeys[inIndex].events[0].ki.time			= 0;
				emuKeys[inIndex].events[0].ki.dwExtraInfo	= EXTRA_INFO_CODE;

				emuKeys[inIndex].events[1].type				= INPUT_KEYBOARD;
				emuKeys[inIndex].events[1].ki.wVk			= *inTag;
				emuKeys[inIndex].events[1].ki.wScan			= MapVirtualKeyW(*inTag, MAPVK_VK_TO_VSC);
				emuKeys[inIndex].events[1].ki.dwFlags		= KEYEVENTF_KEYUP;
				emuKeys[inIndex].events[1].ki.time			= 0;
				emuKeys[inIndex].events[1].ki.dwExtraInfo	= EXTRA_INFO_CODE;

				return 1;
			}
		else
			{
				for(unsigned short index = 0; index < L4D2_KEY_TAGS_LEN; ++index)
					{
						if(strcmp(inTag, L4D2KeyTags[index].Tag) == 0)
							{
								if(L4D2KeyTags[index].type == INPUT_KEYBOARD)
									{
										emuKeys[inIndex].eventsCount						= 2;
										emuKeys[inIndex].events								= (INPUT*) malloc(2 * sizeof(INPUT));
		
										emuKeys[inIndex].events[0].type						= INPUT_KEYBOARD;
										emuKeys[inIndex].events[0].ki.wVk					= (WORD)(L4D2KeyTags[index].Data);
										emuKeys[inIndex].events[0].ki.wScan					= MapVirtualKeyW(L4D2KeyTags[index].Data, MAPVK_VK_TO_VSC);
										emuKeys[inIndex].events[0].ki.dwFlags				= L4D2KeyTags[index].dwFlags.dwDown;
										emuKeys[inIndex].events[0].ki.time					= 0;
										emuKeys[inIndex].events[0].ki.dwExtraInfo			= EXTRA_INFO_CODE;
		
										emuKeys[inIndex].events[1].type						= INPUT_KEYBOARD;
										emuKeys[inIndex].events[1].ki.wVk					= (WORD)(L4D2KeyTags[index].Data);
										emuKeys[inIndex].events[1].ki.wScan					= MapVirtualKeyW(L4D2KeyTags[index].Data, MAPVK_VK_TO_VSC);
										emuKeys[inIndex].events[1].ki.dwFlags				= L4D2KeyTags[index].dwFlags.dwUp;
										emuKeys[inIndex].events[1].ki.time					= 0;
										emuKeys[inIndex].events[1].ki.dwExtraInfo			= EXTRA_INFO_CODE;
									}
								else if(L4D2KeyTags[index].type == INPUT_MOUSE)
									{
										if(L4D2KeyTags[index].dwFlags.dwDown == L4D2KeyTags[index].dwFlags.dwUp)
											{
												emuKeys[inIndex].eventsCount				= 1;
												emuKeys[inIndex].events						= (INPUT*) malloc(sizeof(INPUT));

												emuKeys[inIndex].events[0].type				= INPUT_MOUSE;
												emuKeys[inIndex].events[0].mi.dx			= 0;
												emuKeys[inIndex].events[0].mi.dy			= 0;
												emuKeys[inIndex].events[0].mi.mouseData		= L4D2KeyTags[index].Data;
												emuKeys[inIndex].events[0].mi.dwFlags		= L4D2KeyTags[index].dwFlags.dwDown;
												emuKeys[inIndex].events[0].mi.time			= 0;
												emuKeys[inIndex].events[0].mi.dwExtraInfo	= EXTRA_INFO_CODE;
											}
										else
											{
												emuKeys[inIndex].eventsCount				= 2;
												emuKeys[inIndex].events						= (INPUT*) malloc(2 * sizeof(INPUT));

												emuKeys[inIndex].events[0].type				= INPUT_MOUSE;
												emuKeys[inIndex].events[0].mi.dx			= 0;
												emuKeys[inIndex].events[0].mi.dy			= 0;
												emuKeys[inIndex].events[0].mi.mouseData		= L4D2KeyTags[index].Data;
												emuKeys[inIndex].events[0].mi.dwFlags		= L4D2KeyTags[index].dwFlags.dwDown;
												emuKeys[inIndex].events[0].mi.time			= 0;
												emuKeys[inIndex].events[0].mi.dwExtraInfo	= EXTRA_INFO_CODE;

												emuKeys[inIndex].events[1].type				= INPUT_MOUSE;
												emuKeys[inIndex].events[1].mi.dx			= 0;
												emuKeys[inIndex].events[1].mi.dy			= 0;
												emuKeys[inIndex].events[1].mi.mouseData		= L4D2KeyTags[index].Data;
												emuKeys[inIndex].events[1].mi.dwFlags		= L4D2KeyTags[index].dwFlags.dwUp;
												emuKeys[inIndex].events[1].mi.time			= 0;
												emuKeys[inIndex].events[1].mi.dwExtraInfo	= EXTRA_INFO_CODE;
											}
									}

								return 1;
							}
					}

				return 0;
			}
	}

void toUpperCase(char* inChar)
	{
		while(*inChar)
			{
				if(*inChar >= 97 && *inChar <= 122)
					*inChar -= 32;

				inChar = ((char*)((int)(inChar) + sizeof(char)));
			}
	}

LRESULT __stdcall WindowProc	(
									HWND	hWnd,
									UINT	uMsg,
									WPARAM	wParam,
									LPARAM	lParam
								)
	{
		switch(uMsg)
			{
				case WM_KEYDOWN:
					{
						if(wParam == VK_ADD)
							renderPos -= 20;
						else if(wParam == VK_SUBTRACT)
							renderPos += 20;
						else if(wParam == VK_RETURN)
							{
								memset(emuKeys, 0, sizeof(struct L4D2BindEvents)	* EMU_KEYS_LEN);
								freeL4D2CodeSeq(&bndKeys);

								if(!readL4D2Config("config.ecfg", BIND_TYPE_USER_ACTION, BND_ALL_PARAMS_LEN))
									{
										MessageBoxA	(
														hMainWindow,
														L4D2WarningUserBinds,
														L4D2WarningUserBindsCaption,
														MB_OK
													);
									}

								if(!readL4D2Config(cfgPath, BIND_TYPE_USER_KEY, EMU_KEYS_LEN))
									{
										MessageBoxA	(
														hMainWindow,
														L4D2ErrorNoAction,
														L4D2ErrorCaption,
														MB_OK
													);

										PostQuitMessage(0);

										return 0;
									}
							}

						SendMessageW(hWnd, WM_PAINT, 0, 0);

						break;
					}
				case WM_PAINT:
					{
						SetDCBrushColor(hdc, RGB(255, 255, 255));
						SelectObject(hdc, GetStockObject(DC_BRUSH));

						SetDCPenColor(hdc, RGB(255, 255, 255));
						SelectObject(hdc, GetStockObject(DC_PEN));

						Rectangle(hdc, 0, 0, rect.right, rect.bottom);

						SetDCPenColor(hdc, RGB(0, 0, 0));
						SelectObject(hdc, GetStockObject(DC_PEN));

						char buf[15];
						int currPos = renderPos;

						for(unsigned int index = 0; index < bndKeys.codeLength; ++index)
							{
								itoa(bndKeys.codeSeq[index].code, buf, 16);
								TextOutA(hdc, 0, currPos, buf, strlen(buf));
								currPos += 20;
								TextOutA(hdc, 20, currPos, "{", 1);

								for(int cmdIndex = 0; cmdIndex < bndKeys.codeSeq[index].eventCount; ++cmdIndex)
									{
										currPos += 20;

										if(bndKeys.codeSeq[index].index == L4D2_BIND_TYPE_INDEX_NORMAL)
											{
												switch(bndKeys.codeSeq[index].events[cmdIndex])
													{
														case BHOP_BND_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "BHOP_BND_KEY_INDEX", 18);
																break;
															}
														case JROCK_BND_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "JROCK_BND_KEY_INDEX", 19);
																break;
															}
														case FMELEE_BND_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "FMELEE_BND_KEY_INDEX", 20);
																break;
															}
														case BHOP_ON_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "BHOP_ON_KEY_INDEX", 17);
																break;
															}
														case BHOP_OFF_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "BHOP_OFF_KEY_INDEX", 18);
																break;
															}
														case JROCK_ON_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "JROCK_ON_KEY_INDEX", 18);
																break;
															}
														case JROCK_OFF_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "JROCK_OFF_KEY_INDEX", 19);
																break;
															}
														case FMELEE_ON_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "FMELEE_ON_KEY_INDEX", 19);
																break;
															}
														case FMELEE_OFF_KEY_INDEX:
															{
																TextOutA(hdc, 40, currPos, "FMELEE_OFF_KEY_INDEX", 20);
																break;
															}
														default:
																TextOutA(hdc, 40, currPos, "UNKNOWN", 7);
													}
											}
										else
											{
												char checked = 0;

												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "BHOP_BND_KEY_INDEX", 18);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_JROCK_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "JROCK_BND_KEY_INDEX", 19);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "FMELEE_BND_KEY_INDEX", 20);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_ON_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "BHOP_ON_KEY_INDEX", 17);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_BHOP_OFF_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "BHOP_OFF_KEY_INDEX", 18);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_JROCK_ON_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "JROCK_ON_KEY_INDEX", 18);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_JROCK_OFF_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "JROCK_OFF_KEY_INDEX", 19);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_ON_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "FMELEE_ON_KEY_INDEX", 19);
														currPos += 20;
													}
												if(bndKeys.codeSeq[index].events[cmdIndex] & SWITCH_FMELEE_OFF_MASK)
													{
														checked = 1;
														TextOutA(hdc, 40, currPos, "FMELEE_OFF_KEY_INDEX", 20);
														currPos += 20;
													}

												if(checked == 0)
													{
														TextOutA(hdc, 40, currPos, "UNKNOWN", 7);
														currPos += 20;
													}

												TextOutA(hdc, 40, currPos, "***", 3);
											}
									}

								currPos += 20;
								TextOutA(hdc, 20, currPos, "}", 1);
								currPos += 20;
							}

						break;
					}
				case WM_SIZE:
					{
						DeleteDC(memdc);
						DeleteObject(membmp);
						ReleaseDC(hWnd, hdc);

						GetClientRect(hWnd, &rect);

						hdc							= GetDC(hWnd);

						BITMAPINFO	bi;

						memset(&bi, 0, sizeof(bi));

						bi.bmiHeader.biSize			= sizeof(bi.bmiHeader);
						bi.bmiHeader.biBitCount		= 32;
						bi.bmiHeader.biWidth		= rect.right;
						bi.bmiHeader.biHeight		= -rect.bottom;
						bi.bmiHeader.biCompression	= BI_RGB;
						bi.bmiHeader.biPlanes		= 1;

						memdc						= CreateCompatibleDC(hdc);
						membmp						= CreateDIBSection(memdc, &bi, DIB_RGB_COLORS, (void**)&(pixels), NULL, 0);
						
						SelectObject(memdc, membmp);

						break;
					}
				case WM_CREATE:
					{
						GetClientRect(hWnd, &rect);

						hdc							= GetDC(hWnd);

						BITMAPINFO	bi;

						memset(&bi, 0, sizeof(bi));

						bi.bmiHeader.biSize			= sizeof(bi.bmiHeader);
						bi.bmiHeader.biBitCount		= 32;
						bi.bmiHeader.biWidth		= rect.right;
						bi.bmiHeader.biHeight		= -rect.bottom;
						bi.bmiHeader.biCompression	= BI_RGB;
						bi.bmiHeader.biPlanes		= 1;

						memdc						= CreateCompatibleDC(hdc);
						membmp						= CreateDIBSection(memdc, &bi, DIB_RGB_COLORS, (void**)&(pixels), NULL, 0);
						
						SelectObject(memdc, membmp);

						break;
					}
     			case WM_DESTROY:
					{
						DeleteDC(memdc);
						DeleteObject(membmp);
						ReleaseDC(hWnd, hdc);

						PostQuitMessage(0);

						return 0;
					}
			}

		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	}